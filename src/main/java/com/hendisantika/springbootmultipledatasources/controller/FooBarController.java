package com.hendisantika.springbootmultipledatasources.controller;

import com.hendisantika.springbootmultipledatasources.db1.domain.Bar;
import com.hendisantika.springbootmultipledatasources.db1.repository.BarRepository;
import com.hendisantika.springbootmultipledatasources.db2.domain.Foo;
import com.hendisantika.springbootmultipledatasources.db2.repository.FooRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-datasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/20
 * Time: 05.52
 */
@RestController
public class FooBarController {

    private final FooRepository fooRepo;
    private final BarRepository barRepo;

    @Autowired
    FooBarController(FooRepository fooRepo, BarRepository barRepo) {
        this.fooRepo = fooRepo;
        this.barRepo = barRepo;
    }

    @RequestMapping("/foobar/{id}")
    public String fooBar(@PathVariable("id") Long id) {
        Optional<Foo> fooOptional = fooRepo.findById(id);
        Foo foo = fooOptional.get();

        Optional<Bar> barOptional = barRepo.findById(id);
        Bar bar = barOptional.get();
        return foo.getFoo() + " " + bar.getBar() + "! " + new Date();
    }
}
