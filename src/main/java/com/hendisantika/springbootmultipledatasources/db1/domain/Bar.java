package com.hendisantika.springbootmultipledatasources.db1.domain;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-datasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/20
 * Time: 05.43
 */
@Entity
@Table(name = "bar")
public class Bar {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bar_id_seq")
    @SequenceGenerator(name = "bar_id_seq", sequenceName = "bar_id_seq", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @Column(name = "BAR")
    private String bar;

    public Bar(String bar) {
        this.bar = bar;
    }

    public Bar() {
        // Default constructor needed by JPA
    }

    public String getBar() {
        return bar;
    }
}