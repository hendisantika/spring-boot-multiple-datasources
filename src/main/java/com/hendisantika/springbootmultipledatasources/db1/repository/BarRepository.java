package com.hendisantika.springbootmultipledatasources.db1.repository;

import com.hendisantika.springbootmultipledatasources.db1.domain.Bar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-datasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/20
 * Time: 05.44
 */
@Repository
public interface BarRepository extends JpaRepository<Bar, Long> {

    Optional<Bar> findById(Long id);

}