package com.hendisantika.springbootmultipledatasources.db2.domain;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-datasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/20
 * Time: 05.46
 */
@Entity
@Table(name = "foo")
public class Foo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "foo_id_seq")
    @SequenceGenerator(name = "foo_id_seq", sequenceName = "foo_id_seq", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FOO")
    private String foo;

    public Foo(String foo) {
        this.foo = foo;
    }

    public Foo() {
        // Default constructor needed by JPA
    }

    public String getFoo() {
        return foo;
    }

}
