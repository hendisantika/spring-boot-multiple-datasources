package com.hendisantika.springbootmultipledatasources.db2.repository;

import com.hendisantika.springbootmultipledatasources.db2.domain.Foo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-multiple-datasources
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 07/01/20
 * Time: 05.47
 */
@Repository
public interface FooRepository extends JpaRepository<Foo, Long> {

    Optional<Foo> findById(Long id);

}
